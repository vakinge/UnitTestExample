/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.jeesuite.example.UserNotExistsException;
import com.jeesuite.example.dao.UserDao;
import com.jeesuite.example.model.User;
import com.jeesuite.example.service.UserService;

/**
 * mock测试
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月29日
 */
public class MockTest {

	@Rule
	public ExpectedException thrown= ExpectedException.none();
	
	@Mock
	private UserDao userDao;

	private UserService userService;

	@Before
	public void setUp() throws Exception {
		//生成UserDao mock对象
		MockitoAnnotations.initMocks(this);
		userService = new UserService();
		userService.setUserDao(userDao);
		//定制mock对象预期执行结果
		when(userDao.findByName("大毛")).thenReturn(new User(1000, "大毛"));
		when(userDao.findByName("二毛")).thenReturn(null);
	}

	@Test
	public void shouldFindUserNotNull() {
		User user = userService.findByName("大毛");
		assertEquals("大毛", user.getName());
		assertEquals(1000, user.getId());
	}
	
	@Test
	public void shouldFindUserNull() {
		thrown.expect(UserNotExistsException.class);
		User user = userService.findByName("二毛");
		System.out.println("不会执行到这行"+user);
	}
	
}
