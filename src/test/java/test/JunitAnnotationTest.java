/**
 * 
 */
package test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.jeesuite.example.service.CalculationService;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月16日
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JunitAnnotationTest {

	private CalculationService service = new CalculationService();

	@BeforeClass
	public static void testBeforeClass() {
		System.out.println("注解BeforeClass,在类初始化会执行一次，必须是static");
	}

	@AfterClass
	public static void testAfterClass() {
		System.out.println("注解AfterClass,在所有单元测试执行完，执行一次，必须是static");
	}
	
	@Before
	public void testBefore() {
		System.out.println("注解Before,每一个单元测试方法前执行");
	}

	@After
	public void testAfter() {
		System.out.println("注解After,每一个单元测试方法前执行");
	}
	
	@Test
	public void testA() {
		System.out.println("注解FixMethodOrder,保证方法执行顺序。");
	}
	
	@Test(timeout = 3000)
	public void testTimeout() {
		System.out.println("如果在3秒内没返回就会报错");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExpectException() {
		System.out.println("service.calcPersonTax(0) 会抛IllegalArgumentException 异常");
		service.calcPersonTax(0);
	}

	@Test
	//@Ignore
	public void testIgnore() {
		System.out.println("注解Ignore,忽略该方法执行，改注解在class上整个类都忽略");
	}
}
