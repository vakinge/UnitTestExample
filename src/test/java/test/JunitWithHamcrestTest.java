/**
 * 
 */
package test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月31日
 */
public class JunitWithHamcrestTest {

	@Test
	public void assertThatIncludesDescriptionOfTestedValueInErrorMessage() {
		String expected = "expected";
		String actual = "actual";

		String expectedMessage = "identifier\nExpected: \"expected\"\n     but: was \"actual\"";

		try {
			assertThat("identifier", actual, equalTo(expected));
		} catch (AssertionError e) {
			assertEquals(expectedMessage, e.getMessage());
		}
	}

}
