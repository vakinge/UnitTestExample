/**
 * 
 */
package test;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;
import static org.junit.Assume.assumeThat;

import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.junit.runner.RunWith;

import com.jeesuite.example.service.EmailService;

@RunWith(Theories.class)
public class JunitTheoryTest {

	private static EmailService service = new EmailService();
	
	@Theory
	public final void test(@TestedOn(ints = { 0, 1, 2 }) int i) {
		assertTrue(i >= 0);
	}

	@DataPoints
	public static String[] names() {
		return new String[] { "first", "second", "abc", "123", null };
	}

	@DataPoint
	public static String name = "mary";

	@Theory
	public void testCreateEmailID(String firstPart, String secondPart) throws Exception {
		System.out.println(String.format("Testing with %s and %s", firstPart, secondPart));
		assumeNotNull(firstPart, secondPart);
		assumeThat(firstPart, notNullValue());
		assumeThat(secondPart, notNullValue());
		String actual = service.createEmailID(firstPart, secondPart);
		System.out.println(String.format("Actual: %s \n", actual));
		assertThat(actual, is(allOf(containsString(firstPart), containsString(secondPart))));
	}
}
