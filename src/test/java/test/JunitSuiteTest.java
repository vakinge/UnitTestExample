/**
 * 
 */
package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	CalculationServiceTest.class, 
	JunitAnnotationTest.class
})

public class JunitSuiteTest {

}
