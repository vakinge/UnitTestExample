/**
 * 
 */
package test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.jeesuite.example.service.EmailService;
@RunWith(value = Parameterized.class)
public class JunitParametersTest {

	private static EmailService service = new EmailService();
	
    private String emailId;
    private boolean expected;

    public JunitParametersTest(String emailId, boolean expected) {
        this.emailId = emailId;
        this.expected = expected;
    }
    
    @Parameterized.Parameters(name= "{index}: isValid({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {"damao@163.com", true},
                        {"ermao@qq.com", true},
                        {"danmao@sina.com", true},
                        {"xxxx111", false}
                }
        );
    }
    @Test
    public void testIsValidEmail() throws Exception {
        boolean actual= service.isValidEmail(emailId);
        assertThat(actual, is(equalTo(expected)));
    }
    
    
}
