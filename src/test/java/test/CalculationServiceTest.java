/**
 * 
 */
package test;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeNotNull;
import static org.junit.Assume.assumeThat;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.jeesuite.example.service.CalculationService;

import junit.framework.TestCase;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月16日
 */

@RunWith(Theories.class)
public class CalculationServiceTest extends TestCase{

	private CalculationService service = new CalculationService();
	
	@DataPoints
	public static float[] amounts() {
		return new float[] { 1000, 3500, 9000, 15000 };
	}
	
	@Theory
	public void testCalcPersonTax2(float amount) throws Exception {
		System.out.println(String.format("Testing with %s", amount));
		float result = service.calcPersonTax(amount);
		System.out.println(String.format("Actual: %s \n", result));
	}
}
