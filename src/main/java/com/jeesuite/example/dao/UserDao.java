/**
 * 
 */
package com.jeesuite.example.dao;

import com.jeesuite.example.model.User;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月30日
 */
public interface UserDao {

	User getById(int id);
	User findByName(String name);
	
	int save(User user);
}
