/**
 * 
 */
package com.jeesuite.example;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月30日
 */
public class UserNotExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserNotExistsException() {
		super("用户不存在");
	}

	
}
