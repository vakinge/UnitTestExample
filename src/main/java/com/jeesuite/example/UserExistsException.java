/**
 * 
 */
package com.jeesuite.example;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月30日
 */
public class UserExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserExistsException() {
		super("用户已存在");
	}

	
}
