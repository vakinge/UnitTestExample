/**
 * 
 */
package com.jeesuite.example.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月31日
 */
public class EmailService {

	public String createEmailID(String firstPart, String secondPart) {
		String generatedId = firstPart + "." + secondPart + "@domain.com";
		return generatedId;
	}
	
	public boolean isValidEmail(String email){
        String regex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(email);
        return m.matches();
    }
}
