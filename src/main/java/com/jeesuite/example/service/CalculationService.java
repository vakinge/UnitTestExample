/**
 * 
 */
package com.jeesuite.example.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月16日
 */
public class CalculationService {

	/**
	 * 计算个税（薪金工资）
	 * @param amount
	 * @return
	 */
	public float calcPersonTax(float amount) {
		if(amount <= 0)throw new IllegalArgumentException("金额必须大于0");
		// 扣税公式是：（扣除社保医保公积金后薪水-个税起征点）*税率-速算扣除数
		BigDecimal taxbase =  new BigDecimal(amount).subtract(new BigDecimal(3500));
		BigDecimal taxrate = null;
		BigDecimal deduction = null;
		if (taxbase.floatValue() <= 0){// 低于个税起征点
			return 0;
		} else if (taxbase.floatValue() <= 1500) {
			taxrate = new BigDecimal(0.03);
			deduction = BigDecimal.ZERO;
		} else if (taxbase.floatValue() <= 4500) {
			taxrate = new BigDecimal(0.1);
			deduction = new BigDecimal(105);
		} else if (taxbase.floatValue() <= 9000) {
			taxrate = new BigDecimal(0.2);
			deduction = new BigDecimal(555);
		} else if (taxbase.floatValue() <= 35000) {
			taxrate = new BigDecimal(0.25);
			deduction = new BigDecimal(1005);
		} else if (taxbase.floatValue() <= 55000) {
			taxrate = new BigDecimal(0.3);
			deduction = new BigDecimal(2755);
		} else if (taxbase.floatValue() <= 80000) {
			taxrate = new BigDecimal(0.35);
			deduction = new BigDecimal(5505);
		} else {
			taxrate = new BigDecimal(0.45);
			deduction = new BigDecimal(13505);
		}
		BigDecimal tax = taxbase.multiply(taxrate).subtract(deduction);
		return tax.setScale(2, RoundingMode.HALF_UP).floatValue();
	} 
}
