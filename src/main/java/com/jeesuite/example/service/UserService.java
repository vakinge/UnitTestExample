/**
 * 
 */
package com.jeesuite.example.service;

import com.jeesuite.example.UserExistsException;
import com.jeesuite.example.UserNotExistsException;
import com.jeesuite.example.dao.UserDao;
import com.jeesuite.example.model.User;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月30日
 */

public class UserService {

	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public User findByName(String name){
		User user = userDao.findByName(name);
		if(user == null){
			throw new UserNotExistsException();
		}
		return user;
	}
	
	public int add(User user){
		User existUser = userDao.getById(user.getId());
		if(existUser != null){
			throw new UserExistsException();
		}
		return userDao.save(user);
	}
}
